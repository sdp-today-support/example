# Overview

## 介紹
This is an example system with components and resources.

## 慨念
* [Model](https://backstage.io/docs/features/software-catalog/system-model)
* [Entities](https://backstage.io/docs/features/software-catalog/descriptor-format)
* [Entity References](https://backstage.io/docs/features/software-catalog/references)

[![](./media/model.png)](https://backstage.io/docs/features/software-catalog/system-model)

## 架構
![structure](./media/102222.png)
