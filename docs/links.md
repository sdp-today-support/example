# Links

## Repo
* Google Drive
* [GitLab](https://gitlab.com/sdp-today-support/example)

## Resource
* GKE
* Cloud SQL
* Cloud Storage

## Reference
* [backstage.io](https://backstage.io/docs/overview/what-is-backstage)
